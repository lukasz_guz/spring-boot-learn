package com.impaq.controller;

import com.impaq.domain.PropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class SimpleRestController {

    @Value("${impaq.properties.text}")
    private String textFromProperties;

    @Value("${impaq.other.properties.text}")
    private String textFromOtherProperties;

    @Value("${impaq.yml.text}")
    private String textFromYml;

    private PropertiesDto propertiesDto = new PropertiesDto();

    @RequestMapping("/properties")
    public PropertiesDto properties() {
        propertiesDto.setPropertiesText(textFromProperties);
        propertiesDto.setYmlText(textFromYml);
        propertiesDto.setOtherPropertiesText(textFromOtherProperties);
        return propertiesDto;
    }
}