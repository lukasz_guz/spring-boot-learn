package com.impaq.domain;

public class PropertiesDto {

    private String otherPropertiesText;
    private String propertiesText;
    private String ymlText;

    public String getPropertiesText() {
        return propertiesText;
    }

    public void setPropertiesText(String propertiesText) {
        this.propertiesText = propertiesText;
    }

    public String getYmlText() {
        return ymlText;
    }

    public String getOtherPropertiesText() {
        return otherPropertiesText;
    }

    public void setOtherPropertiesText(String otherPropertiesText) {
        this.otherPropertiesText = otherPropertiesText;
    }

    public void setYmlText(String ymlText) {
        this.ymlText = ymlText;
    }
}